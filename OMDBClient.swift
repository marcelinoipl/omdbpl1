//
//  OMDBClient.swift
//  omdbPL1
//
//  Created by Luis Marcelino on 17/11/15.
//  Copyright © 2015 Empresa Imaginada. All rights reserved.
//

import Foundation

class OMDBClient {
    
    static func searchMovieWithKey(query:String, completionHandler: ([Movie]?) -> Void) {
        guard let url = NSURL(string: "http://www.omdbapi.com/?s=" + query) else {
            return
        }
        // url exists!!
        let session = NSURLSession.sharedSession()
        let task = session.dataTaskWithURL(url) { (data, response, error) -> Void in
            if let d = data  {
                // d exists!!!
                let movies = OMDBClient.parseMovies(d)
                completionHandler(movies)
            }
        }
        task.resume()
    }
    
    static func parseMovies(data:NSData) -> [Movie]? {

        do {
            if let jsonResponse = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments) as? NSDictionary {
                // jsonResponse as dictionary exists
                var movies = [Movie]()
                
                if let jsonSearch = jsonResponse.objectForKey("Search") as? NSArray {
                    for (var i = 0 ; i < jsonSearch.count ; i++) {
                        if let jsonMovie = jsonSearch.objectAtIndex(i) as? NSDictionary {
                            let id = jsonMovie.objectForKey("imdbID") as! String
                            let ttl = jsonMovie.objectForKey("Title") as! String
                            let type = jsonMovie.objectForKey("Type") as! String
                            let y = jsonMovie.objectForKey("Year") as! String
                            let p = jsonMovie.objectForKey("Poster") as? String
                            let movie = Movie(imdbId: id, title: ttl, year: y, type: type, poster: p)
                            movies.append(movie)
                        }
                    }
                }
                return movies
            }
        }
        catch  {
            print(error)
        }
        return nil;
    }
}