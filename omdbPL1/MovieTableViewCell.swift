//
//  MovieTableViewCell.swift
//  omdbPL1
//
//  Created by Luis Marcelino on 24/11/15.
//  Copyright © 2015 Empresa Imaginada. All rights reserved.
//

import UIKit

class MovieTableViewCell: UITableViewCell {

    
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var posterImage: UIImageView!
    
    func setMovie (movie:Movie) {
        self.titleLabel.text = movie.title
        self.yearLabel.text = movie.year
        self.typeLabel.text = movie.type
        if let posterUrl = movie.poster {
            if let url = NSURL(string: posterUrl) {
                let session = NSURLSession.sharedSession()
                session.dataTaskWithURL(url, completionHandler: { (data, response, error) -> Void in
                    if let d = data {
                        NSOperationQueue.mainQueue().addOperationWithBlock({ () -> Void in
                            self.posterImage.image = UIImage(data: d)
                        })
                    }
                }).resume()
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
