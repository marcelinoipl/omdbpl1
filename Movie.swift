//
//  Movie.swift
//  omdbPL1
//
//  Created by Luis Marcelino on 17/11/15.
//  Copyright © 2015 Empresa Imaginada. All rights reserved.
//

import Foundation

struct Movie {
    let imdbId:String
    let title:String
    let year:String
    let type:String
    let poster:String?
}